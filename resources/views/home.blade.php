<x-layout>
<x-slot name="title">Studio Dott. Pinza</x-slot>

@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif

<!-- Header -->
<div class="container-fluid teeth-header">
    <div class="row head-text">
        <div class="col-12">
            <h1>Esperienza e professionalità</h1>
            <a href="{{route('services')}}">
                <button class="teeth-button head-text fw-bold mt-3">
                     Prenota il primo checkup gratis!
                </button>
            </a>
        </div>
    </div>
</div>











</x-layout>    