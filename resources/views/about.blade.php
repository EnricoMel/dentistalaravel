<x-layout>
 <x-slot name="title">Chi siamo</x-slot>

 <div class="container">
       <div class="row">
          <div class="col-12 text-center">
            <h1 class="text-center fw-bold mt-4">Il nostro team</h1>
          </div>
        </div>
    
        <div class="container">
            <div class="row">
            
               @foreach ($team as $membro)
                  
               <div class="col-12 col-md-3 text-center my-5">
               <x-card
               key1="{{$membro['nome']}}"
               key2="{{$membro['cognome']}}"
               key3="{{$membro['specializzazione']}}"
               keyimg="{{$membro['img']}}"
               />
               </div>     
               @endforeach
            </div>
        </div>         
 </div>



</x-layout>    