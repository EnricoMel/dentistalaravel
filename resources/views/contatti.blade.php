<x-layout>
     <x-slot name="title">Contattaci</x-slot>

     <div class="container">
         <div class="row">
               <div class="col-12 text-center mt-4">
                 <h1 class="fw-bolder">Contattaci</h1>
                </div>
          </div>

                 @if ($errors->any())
                      <div class="alert alert-danger errors-window fw-bolder">
                         <ul>
                            @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                      </div>
                @endif

         <div class="row justify-content-center mb-5">
             <div class="col-12 col-md-4">
                <form method="POST" action="{{route('store')}}">
                   @csrf
                    <div class="mb-3">
                      <label for="exampleInputName" class="form-label">Nome e cognome</label>
                      <input type="text" class="form-control" id="exampleInputName" aria-describedby="emailHelp" name="name" value="{{old('name')}}">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail" class="form-label">Email</label>
                        <input type="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" name="email" value="{{old('email')}}">
                        <div id="emailHelp" class="form-text">Non condivideremo la tua email con nessuno.</div>
                      </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Inserisci un messaggio</label>
                      <textarea class="form-text-area" name="message" id="exampleInputPassword1" cols="48" rows="5" placeholder="Scrivi...">{{old('message')}}</textarea>
                    </div>
                    
                    <button type="submit" class="card-button">Invia</button>

                </form>

            </div>

         </div>
     </div>
   
   





























</x-layout>    
