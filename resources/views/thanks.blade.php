<x-layout>
    <x-slot name="title">Thank you!</x-slot>

    <div class="container-fluid teeth-header">
        <div class="row head-text">
            <div class="col-12">
                <h1>Grazie per averci contattato</h1>
                <a href="{{route('home')}}">
                    <button class="teeth-button head-text fw-bold mt-3">
                         Torna alla home!
                    </button>
                </a>
            </div>
        </div>
    </div>





</x-layout>    