{{-- Footer --}}
<footer class="teeth-footer">
    <div class="container">
        <div class="row text-center">
            <div class="col-12 col-md-4">
                <h3 class="head-text fw-bold">
                    Un pò di noi
                </h3>
                <p>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                    Laborum pariatur repellat expedita molestias eligendi aperiam saepe maiores. 
                    Architecto mollitia eos blanditiis, sed, nisi id, deserunt beatae ullam unde explicabo voluptatem!
                </p> 
            </div>
            <div class="col-12 col-md-4">
                <h3 class="head-text fw-bold">
                    Link
                </h3>
                <p>Link 1</p>
                <p>Link 2</p>
                <p>Link 3</p>
                <p>Link 4</p>
            </div>          
            <div class="col-12 col-md-4">
                <div class="d-flex flex-wrap">

                    <a class="text-decoration-none" href="#">
                        <div class="teeth-footer-icon">
                            <i class="fab fa-facebook-f fa-2x"></i>
                        </div>   
                    </a>

                    <a class="text-decoration-none" href="#">
                        <div class="teeth-footer-icon">
                            <i class="fab fa-twitter fa-2x"></i>
                        </div>   
                    </a>
                    
                    <a class="text-decoration-none" href="#">
                        <div class="teeth-footer-icon">
                            <i class="fab fa-instagram fa-2x"></i>
                        </div>   
                    </a>    
                </div>                          
            </div>
            <div class="col-12 mt-3 fst-italic text-white">
                <span>Studio dentistico Dott. Pinza</span>
            </div>
            <div class="col-12 fst-italic text-white">
                <span>Via dei sorrisi 100</span>
            </div>  
        </div>
    </div>
</footer>