<div class="card card-style shadow p-3 mb-5 bg-body rounded" style="width: 18rem;">
  <img src="{{$keyimg ?? ''}}" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">{{$key1}}</h5>
    <p class="card-text mb-5">{{$key3}}</p>
    <a href="#" class="card-button text-decoration-none">Vai alla scheda</a>
  </div>
</div>