<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="text-center fw-bold mt-4">Tutti i contatti ricevuti</h1>
            </div>
        </div>

        <div class="container">
            <div class="row">

                @foreach ($contacts as $contact)

                <div class="col-12 col-md-3 text-center my-5">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">{{$contact->name}}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{$contact->email}}</h6>
                            <p class="card-text">{{$contact->message}}</p>
                            <a href="#" class="card-link">Card link</a>
                            <a href="#" class="card-link">Other links</a>
                        </div>
                    </div>

                </div>
                @endforeach
            </div>
        </div>
    </div>








</x-layout>