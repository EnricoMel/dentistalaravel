<x-layout>
<x-slot name="title">I nostri servizi</x-slot>

   <div class="container">
       <div class="row">
           <div class="col-12 text-center">
               <h1 class="text-center fw-bold mt-4">I nostri servizi</h1>
           </div>
       </div>

       <div class="row justify-content-center">

           @foreach ($services as $service)
               <div class="col-12 col-md-3 my-5">
                   <x-card
                     key1="{{$service['tipo']}}"
                     key2="{{$service['dott']}}"
                     key3="{{$service['description']}}"
                     keyimg="{{$service['img']}}"
                     
                   />

               </div>
               
           @endforeach
           
       </div>
   </div>


























</x-layout>