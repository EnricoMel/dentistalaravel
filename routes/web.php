<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\FormController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'HomePage'])->name('home');
Route::get('/servizi', [PublicController::class, 'services'])->name('services');
Route::get('/contatti', [FormController::class, 'form'])->name('form');
Route::post('contatti/submit', [FormController::class, 'store'])->name('store');
Route::get('/thanks', [FormController::class, 'thanks'])->name('thanks');
Route::get('/chi-siamo', [PublicController::class, 'about'])->name('about');
Route::get('/tutti-i-contatti', [FormController::class, 'index'])->name('allcontacts');




