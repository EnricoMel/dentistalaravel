<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id(); // questa colonna di nome id avrà come tipo di dato un numero intero, positivo, autoincrementale
            $table->string('email');
            $table->string('name');
            $table->text('message');
            $table->timestamps();    // funzione che mi permette di creare due colonne (l'unica), memorizziamo la data di creazione e di modifica.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
