<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function HomePage() {
        return view('home');
    }

    public function services() {
        $services = [
            ['tipo'=>'Implantologia', 'dott'=>'Roberto Pinza', 'description'=>'Sostituzione di elementi dentali compromessi o già mancanti, con impianti in titanio.', 'img'=>'/img/implantologia.jpg'],
            ['tipo'=>'Protesi dentaria', 'dott'=>'Luigi Terra', 'description'=>'Si occupa di sostituire elementi dentali mancanti con manufatti più o meno estesi.', 'img'=>'/img/protesidentale.jpg'],
            ['tipo'=>'Ortodonzia', 'dott'=>'Lucia Caraffa', 'description'=>'L’ortodonzia è quella branca rivolta alla correzione di malposizioni dei denti.', 'img'=>'/img/ortodonzia.jpg'],
            ['tipo'=>'Igiene', 'dott'=>'Maria Selenio', 'description'=>'E’ la branca dell’odontoiatria che si occupa della motivazione all’igiene del paziente.', 'img'=>'/img/igienista.jpg']

        ];
        return view('servizi', compact('services'));
    }

    public function about() {
        $team = [
            ['nome'=>'Roberto', 'cognome'=>'Pinza', 'eta'=>'54', 'specializzazione'=>'Dentista', 'img'=>'/img/Roberto.jpg'],
            ['nome'=>'Luigi', 'cognome'=>'Terra', 'eta'=>'50', 'specializzazione'=>'Odontoiatra', 'img'=>'/img/Luigi.jpg'],
            ['nome'=>'Lucia', 'cognome'=>'Caraffa', 'eta'=>'45', 'specializzazione'=>'Odontotecnico', 'img'=>'/img/Lucia.jpg'],
            ['nome'=>'Maria', 'cognome'=>'Selenio', 'eta'=>'28', 'specializzazione'=>'Igienista', 'img'=>'/img/Maria.jpg']

        ];

        return view('about', compact('team'));
    }

    public function details() {
        return view('specifica');
    }
    
}

