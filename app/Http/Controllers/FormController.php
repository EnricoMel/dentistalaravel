<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRequest;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    public function form() {
        return view('contatti');
    }

    public function store(StoreRequest $req) { // Dep-in
            /* dd($req->all()); */
            
            
                $contact = Contact::create([
                    'email'=> $req->email,
                    'name'=> $req->name,
                    'message'=> $req->message
                ]);

           
               
            
            /* 
            $contact->email = $req->input('email');
            $contact->name = $req->input('name');
            $contact->message = $req->input('message');
            $contact->save(); */
        


            Mail::to($contact->email)->send(new ContactMail($contact));

            /* return redirect (route('home'))->with('message', 'Grazie per averci contattato!'); // sessione, viaggia un messaggio nel redirect, che poi stamperò. */

            return redirect(route('thanks')); // rotta get

    }

    public function thanks() {
        return view('thanks');
    }

    public function index(){

        $contacts = Contact::all();
        return view('allContacts', compact('contacts'));
    }

    
}
