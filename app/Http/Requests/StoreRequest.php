<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:5|max:30',
            'email'=>'required|email:rfc,dns',
            'message'=>'required|min:30|max:100'

        ];
    }

    public function messages() {
          return [
              'name.required'=> 'Non hai inserito un nome',
              'name.min'=> 'Il nome deve contenete almeno 5 caratteri',
              'name.max'=> 'Il nome non può superare i 30 caratteri',
              'email.required'=> 'Non hai inserito la mail',
              'email.rfc'=> 'La mail non è corretta, inserire @',
              'email.dns'=>'La mail non è corretta',
              'message.required'=>'Inserire un messaggio',
              'message.min'=>'Messaggio troppo corto, inserire minimo 30 caratteri',
              'message.max'=>'Messaggio troppo lungo, inserire massimo 100 caratteri',
              
              
          ];
    }
}
